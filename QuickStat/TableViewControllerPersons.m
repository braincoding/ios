//
//  TableViewControllerPersons.m
//  QuickStat
//
//  Created by Admin on 12.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import "TableViewControllerPersons.h"
#import "Constants.h"


@implementation TableViewControllerPersons


- (void)viewDidLoad {
    [super viewDidLoad];
    
    model = [Model sharedInstance];
    
    NSData* anData = [model request:urlPrefixPersons delegate:self];
    [model savePersonsData:anData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [model.persons count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellPersons" forIndexPath:indexPath];
    NSDictionary *anDict = model.persons[indexPath.row];
    cell.textLabel.text = [anDict objectForKey:@"name"];
    
    return cell;
}


@end
