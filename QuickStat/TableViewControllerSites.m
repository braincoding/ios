//
//  TableViewControllerSites.m
//  QuickStat
//
//  Created by Admin on 12.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import "TableViewControllerSites.h"
#import "Constants.h"


@implementation TableViewControllerSites


- (void)viewDidLoad {
    [super viewDidLoad];
    
    model = [Model sharedInstance];
    
    NSData* anData = [model request:urlPrefixSites delegate:self];
    [model saveSitesData:anData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [model.sites count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSites" forIndexPath:indexPath];
    NSDictionary *anDict = model.sites[indexPath.row];
    cell.textLabel.text = [anDict objectForKey:@"name"];
    
    return cell;
}


@end
