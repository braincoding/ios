//
//  logoSplashViewController.m
//  QuickStat
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import "logoSplashViewController.h"

@interface logoSplashViewController ()

@end

@implementation logoSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- ( void ) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:1.0 delay:0.2 options:0
                     animations:^
     {
//         CGRect frame = self.view.frame;
//         frame.origin.y = frame.origin.y / 5;
//         frame.size.width = frame.size.width / 5;
//         frame.size.height = frame.size.height / 5;
//         frame.origin.x = (self.view.window.frame.size.width / 2) - (frame.size.width / 2);
//         self.view.frame = frame;
     }
                     completion:^( BOOL completed )
     {
         [self performSegueWithIdentifier:@"splash" sender:self];
     }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [segue destinationViewController].modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
}


@end
