//
//  Model.m
//  QuickStat
//
//  Created by Admin on 07.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@implementation Model

+ (instancetype)sharedInstance {
    static Model *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Model alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.persons = [[NSMutableArray alloc] init];
        self.sites = [[NSMutableArray alloc] init];
        self.allStats = [[NSMutableArray alloc] init];
        self.dayStat = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSData*) request:(NSString*) urlString delegate:(UITableViewController*) sender {
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    //NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:sender delegateQueue:nil];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    NSURL* url = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    __block NSData* anData;
    
    // fix: realise through delegate
    NSURLSessionDataTask* datatask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil) {
            anData = data;
            NSString* text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"log: %@", response.MIMEType);
            //NSLog(@"log: %@", obj);
            
            dispatch_group_leave(group);
        }
    }];
    
    [datatask resume];
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    return anData;
    
    //UITableViewController* anSender = sender;
    //[sender.tableView reloadData];
    
}

- (void) saveSitesData:(NSData*) anData {
    
    NSArray * json = [NSJSONSerialization JSONObjectWithData:anData options:0 error:nil];
    if ([NSJSONSerialization isValidJSONObject:json])
    {
        for (NSDictionary *i in json) {
            NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:[i valueForKey:@"id"],@"id",[i valueForKey:@"name"],@"name",nil];
            [self.sites addObject:dict];
        }
        
    }
}

- (void) savePersonsData:(NSData*) anData {
    
    NSArray * json = [NSJSONSerialization JSONObjectWithData:anData options:0 error:nil];
    if ([NSJSONSerialization isValidJSONObject:json])
    {
        for (NSDictionary *i in json) {
            NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:[i valueForKey:@"id"],@"id",[i valueForKey:@"name"],@"name",nil];
            [self.persons addObject:dict];
        }
        
    }
}

- (void) saveAllStatsData:(NSData*) anData {
    
    NSArray * json = [NSJSONSerialization JSONObjectWithData:anData options:0 error:nil];
    if ([NSJSONSerialization isValidJSONObject:json])
    {
        for (NSDictionary *i in json) {
            NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:[i valueForKey:@"id"],@"id",[i valueForKey:@"name"],@"name",[i valueForKey:@"rank"],@"rank",nil];
            [self.allStats addObject:dict];
        }
        
    }
}

@end
