//
//  Model.h
//  QuickStat
//
//  Created by Admin on 07.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject {
    
}

@property (atomic, strong) NSMutableArray* sites;
@property (atomic, strong) NSMutableArray* persons;
@property (atomic, strong) NSMutableArray* allStats;
@property (atomic, strong) NSMutableArray* dayStat;

+ (instancetype)sharedInstance;
- (id)init;
- (NSData*) request:(NSString*) urlString delegate:(id) sender;
- (void) saveSitesData:(NSData*) anData;
- (void) savePersonsData:(NSData*) anData;
- (void) saveAllStatsData:(NSData*) anData;

@end
