//
//  Constants.h
//  QuickStat
//
//  Created by Admin on 08.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// URL prefix set up there
#define urlPrefixSites @"http://artmisha.ru/rest_php/v1/sites"
#define urlPrefixPersons @"http://artmisha.ru/rest_php/v1/persons"
#define urlPrefixStats @"http://artmisha.ru/rest_php/v1/stats"
#define urlPrefixKeywords @"http://artmisha.ru/rest_php/v1/keywords"
#define urlPic @"http://artmisha.ru/wp-content/uploads/2015/10/%D0%90%D0%BB%D0%B8%D1%81%D0%B0.jpg"


#endif /* Constants_h */
