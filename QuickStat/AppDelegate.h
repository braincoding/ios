//
//  AppDelegate.h
//  QuickStat
//
//  Created by Admin on 02.07.16.
//  Copyright © 2016 Boris-Mikhalev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

